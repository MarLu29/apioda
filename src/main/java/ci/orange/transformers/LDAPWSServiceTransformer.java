package ci.orange.transformers;

import java.util.HashMap;

import org.mule.api.MuleMessage;
import org.mule.api.transformer.TransformerException;
import org.mule.transformer.AbstractMessageTransformer;

public class LDAPWSServiceTransformer extends AbstractMessageTransformer {

	@Override
	public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
		HashMap<?,?> params = (HashMap<?, ?>) message.getPayload();
		String username = (String)params.get("username");
		String password = (String)params.get("password");
		
		String[] array = new String[2];
		
		array[0] = username;
		array[1] = password;
		
		return array;
		
	}

}
