package ci.orange.api.interceptor;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.apache.log4j.Logger;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.processor.LoggerMessageProcessor;
import org.mule.api.transport.PropertyScope;
import org.mule.interceptor.AbstractEnvelopeInterceptor;
import org.mule.management.stats.ProcessingTime;
import org.mule.module.http.internal.ParameterMap;
import org.mule.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ci.orange.api.audit.ApiCaller;
import ci.orange.api.audit.ApiContextError;
import ci.orange.api.audit.ApiContextError.ErrorType;
import ci.orange.api.audit.AuditEvent;
import ci.orange.api.common.exceptions.ApiException;

public class AuditInterceptor extends AbstractEnvelopeInterceptor  {
	private static Logger logger = Logger.getLogger(LoggerMessageProcessor.class.getCanonicalName());

	@Override
	public MuleEvent before(MuleEvent event) throws MuleException {
		Long matchingKey;
		UUID statisticid;
		
		if (((ParameterMap)event.getMessage().getInboundProperty("http.query.params")).get("MatchingKey") != null) {
			matchingKey = Long.valueOf(event.getMessage().getInboundProperty("http.query.params").toString());
		} else {
			matchingKey = new java.util.Date().getTime();
		}
		
		if (event.getMessage().getInboundProperty("statisticid") != null) {
			statisticid = UUID.fromString(event.getMessage().getInboundProperty("statisticid").toString());
		} else {
			statisticid = java.util.UUID.randomUUID();
		}
		
		Map<String, Object> mapProperties =  new HashMap<String, Object>();
		mapProperties.put("MatchingKey", matchingKey);
		mapProperties.put("statisticid", statisticid);
				
		String[] splitReqPath = StringUtils.split(event.getMessage().getInboundProperty("http.request.path").toString(), "/");
		String[] splitRemoteAddressArray = StringUtils.split(event.getMessage().getInboundProperty("http.remote.address").toString());
		String[] splitRemoteAddress = StringUtils.split(splitRemoteAddressArray[0], ":");
		AuditEvent auditEvent = new ci.orange.api.audit.AuditEvent(String.valueOf(matchingKey), splitReqPath[1]);
		ApiCaller apiCaller = new ci.orange.api.audit.ApiCaller();
		apiCaller.setIp(splitRemoteAddress[0]);
		apiCaller.setPort(splitRemoteAddress[1]);
		if (event.getMessage().getInboundProperty("user-agent") != null) {
			apiCaller.setPlatform(event.getMessage().getInboundProperty("user-agent").toString());
		}
		if (event.getMessage().getInboundProperty("Authorization") != null) {
			apiCaller.setLogin(event.getMessage().getInboundProperty("Authorization").toString());
		}
		auditEvent.setCaller(apiCaller);
		auditEvent.updateUseCase(splitReqPath[splitReqPath.length - 1], null);
		
		mapProperties.put("auditEvent", auditEvent);
		
		event.getMessage().addProperties(mapProperties, PropertyScope.INVOCATION);
		
		return event;
	}

	@Override
	public MuleEvent after(MuleEvent event) throws MuleException {
		return event;
	}

	@Override
	public MuleEvent last(MuleEvent event, ProcessingTime time, long startTime, boolean exceptionWasThrown)
			throws MuleException {
		
		AuditEvent auditEvent = (AuditEvent)event.getMessage().getInvocationProperty("auditEvent");
		
		if (exceptionWasThrown) {
			if (event.getMessage().getExceptionPayload() != null) {
				if (event.getMessage().getExceptionPayload().getRootException().getClass().getName() == "ci.orange.api.common.exceptions.ApiException") {
				
					ApiException apiException = (ApiException)event.getMessage().getInvocationProperty("apiException");
					ApiContextError apiContextError = new ApiContextError();
					apiContextError.setCode(apiException.getCode());
					apiContextError.setMessage(apiException.getGenericMessage());
					if (apiException.getCode() >= 4000 &&  apiException.getCode() < 5000)
						apiContextError.setType(ErrorType.FUNCT);
					else
						apiContextError.setType(ErrorType.TECH);
					auditEvent.setError(apiContextError);
				} else {
					ApiContextError apiContextError = new ApiContextError();
					apiContextError.setMessage(event.getMessage().getExceptionPayload().getMessage());
					apiContextError.setCode(event.getMessage().getExceptionPayload().getCode());
					apiContextError.setType(ErrorType.TECH);
					auditEvent.setError(apiContextError);
				}
			} 
		}
		
		auditEvent.setExitDate(java.util.Calendar.getInstance().getTimeInMillis());
		ObjectMapper objectMapper = new ObjectMapper();
		
		try {
			logger.info(objectMapper.writeValueAsString(auditEvent));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return event;
	}

}
