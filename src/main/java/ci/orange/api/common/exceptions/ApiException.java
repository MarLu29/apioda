package ci.orange.api.common.exceptions;

public class ApiException extends Exception {
	private static final long serialVersionUID = 1L;

	private Integer code;

    private String genericMessage;

    protected Object[] arguments = null;

    protected String suffix = "";

    public ApiException() {
        super();
    }

    public ApiException(String message, Throwable cause) {
        super(message, cause);
        arguments = new Object[] { message };
    }

    public ApiException(String message) {
        super(message);
        arguments = new Object[] { message };
    }

    public ApiException(Throwable cause) {
        super(cause);
        arguments = new Object[] { cause.getMessage() };
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getGenericMessage() {
        return genericMessage;
    }

    public void setGenericMessage(String genericMessage) {
        this.genericMessage = genericMessage;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public String getSuffix() {
        return suffix;
    }
}
