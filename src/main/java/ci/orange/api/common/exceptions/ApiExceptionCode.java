package ci.orange.api.common.exceptions;

public class ApiExceptionCode {
	// technical
    public static Integer TC_GENERIC = 5000;
    public static Integer TC_ENUM = 5001;
    public static Integer TC_PENDING_REQUEST = 5002;
    public static Integer TC_NOT_IMPLEMENTED = 5003;
    public static Integer TC_WEBSERVICE = 5300;

    // functionnal
    public static Integer FC_GENERIC = 4000;
    public static Integer FC_NOT_FOUND = 4040;
    public static Integer FC_EMPTY_PARAMETER = 4001;
    public static Integer FC_BAD_FORMAT_PARAMETER = 4002;
    public static Integer FC_BAD_VALUE_PARAMETER = 4003;
    public static Integer FC_BAD_PARAMETERS = 4005;
    public static Integer FC_TOO_MANY_PARAMETERS = 4006;
    public static Integer FC_LOGIN_PWD_INCORRECT = 4007;
    public static Integer FC_LOGIN_NOT_AUTH = 4008;
    public static Integer FC_LOGIN_ALLREADY_CONNECTED = 4009;
    public static Integer FC_TOKEN_NOT_FOUND = 4010;
    public static Integer FC_PURCHASE_BALANCE_INSUFFICIENT = 4011;
    public static Integer FC_PURCHASE_HIGH_EQUIPMENT_NOT_ALLOW = 4012;
    public static Integer FC_PURCHASE_QUOTA_OVERLOAD = 4013;
    public static Integer FC_PURCHASE_NO_STOCK_AVAILABLE = 4014;
    public static Integer FC_PURCHASE_QTE_OVERLOAD = 4015;
    public static Integer FC_ACCOUNT_ALLREADY_EXIST = 4016;
}
