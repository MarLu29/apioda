package ci.orange.api.audit;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class AuditEvent {
	private String transactionId;

    private ApiCaller caller;

    private ApiStats stats;

    private ApiUseCase useCase;

    private ApiContextError error;
    
    public AuditEvent() {
        transactionId = String.valueOf(new java.util.Date().getTime());
        stats = new ApiStats();
        useCase = new ApiUseCase();
    }
    
    public AuditEvent(String session_id) {
        transactionId = session_id;
        stats = new ApiStats();
        useCase = new ApiUseCase();
    }

    public AuditEvent(String session_id, String apiName) {
        transactionId = session_id;
        stats = new ApiStats();
        useCase = new ApiUseCase(apiName);
    }
    
    public String getTransactionID() {
        return transactionId;
    }

    public ApiUseCase getUseCase() {
        return useCase;
    }

    public ApiCaller getCaller() {
        return caller;
    }

    public void setCaller(ApiCaller caller) {
        this.caller = caller;
    }

    public ApiStats getStats() {
        return stats;
    }

    public ApiContextError getError() {
        return error;
    }

    public void setError(ApiContextError error) {
        this.error = error;
    }

    public void setExitDate(long exitDate) {
        stats.setExitDate(exitDate);
    }

    public void updateUseCase(String method, String msisdn) {
        useCase.setMethod(method);
        useCase.setMsisdn(msisdn);
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
