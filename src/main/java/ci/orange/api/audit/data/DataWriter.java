package ci.orange.api.audit.data;

import org.apache.log4j.Logger;
import org.mule.api.processor.LoggerMessageProcessor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DataWriter {
	private static Logger logger = Logger.getLogger(LoggerMessageProcessor.class.getCanonicalName());
	
	public static void write(String statisticid, String apiName, String action, String matchingKey, String publicKey, String data) {
		AuditData auditData = new AuditData(statisticid, apiName, action, matchingKey, publicKey, data);
		ObjectMapper objectMapper = new ObjectMapper();
		try {
			logger.info(objectMapper.writeValueAsString(auditData));
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
	}

}
