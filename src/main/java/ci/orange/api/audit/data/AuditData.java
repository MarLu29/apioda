package ci.orange.api.audit.data;

import java.net.UnknownHostException;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class AuditData {
	private String statistic_id;

    private DataNode node;

    private DataStats stats;

    private DataPayload dataPayload;

    public AuditData(String statistic_id, String nodeName, String action, String matchingKey, String publicKey, String data) {
    	this.statistic_id = statistic_id;
    	DataNode dNode = new DataNode();
    	try {
			dNode.setHost(java.net.InetAddress.getLocalHost().getHostName());
		} catch (UnknownHostException e) {
			dNode.setHost("unknow");
		}
    	dNode.setName(nodeName);
    	this.node = dNode;
    	this.stats = new DataStats();
    	DataPayload dDataPayload = new DataPayload();
    	dDataPayload.setAction(action);
    	dDataPayload.setMatchingKey(matchingKey);
    	dDataPayload.setPublicKey(publicKey);
    	dDataPayload.setData(data);
    	this.dataPayload = dDataPayload;
    }
    
	public String getStatistic_id() {
		return statistic_id;
	}

	public void setStatistic_id(String statistic_id) {
		this.statistic_id = statistic_id;
	}

	public DataNode getNode() {
		return node;
	}

	public void setNode(DataNode node) {
		this.node = node;
	}

	public DataStats getStats() {
		return stats;
	}

	public void setStats(DataStats stats) {
		this.stats = stats;
	}

	public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

	public DataPayload getDataPayload() {
		return dataPayload;
	}

	public void setDataPayload(DataPayload dataPayload) {
		this.dataPayload = dataPayload;
	}
}
