package ci.orange.api.audit;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class ApiStats {
	private Long entryDate;

    private Long exitDate;

    private String executionTime;
    
    private Long executionTimeLong;
    
    public ApiStats() {
        entryDate = Calendar.getInstance().getTimeInMillis();
    }
    
    public void setExitDate(Long exitDate) {
        this.exitDate = exitDate;
        this.setExecutionTimeLong(exitDate - entryDate);
        this.executionTime = String.valueOf(exitDate - entryDate) + " ms";
    }
    
    public String getEntryDate() {
        return formatDate(entryDate);
    }

    public Long getEntryDateTime() {
        return entryDate;
    }
    
    public String getExitDate() {
        return formatDate(exitDate);
    }

    private String formatDate(Long exitDate) {
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss z");
        return dateFormat.format(exitDate);
    }
    
    public String getExecutionTime() {
        return executionTime;
    }
    
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }

	public Long getExecutionTimeLong() {
		return executionTimeLong;
	}

	public void setExecutionTimeLong(Long executionTimeLong) {
		this.executionTimeLong = executionTimeLong;
	}
}
